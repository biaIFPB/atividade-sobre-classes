
class Estadio {
  constructor(nome, localizacao, capacidade) {
    this.nome = nome;
    this.localizacao = localizacao;
    this.capacidade = capacidade;
  }
  
  imprimirDadosDoEstadio() {
    console.log(`COPA DO BRASIL
    
Estádio
    
Nome: ${this.nome} 
Localização: ${this.localizacao}
Capacidade: ${this.capacidade}
`);
  }
}

let estadio = new Estadio(" Arena Corinthians ", "São Paulo/SP", "aproximadamente " + 49 + " mil pessoas");
estadio.imprimirDadosDoEstadio();


class Jogador {
  constructor(nome, numeroDaCamisa, posicao, dataDeNascimento) {
    this.nome = nome;
    this.numeroDaCamisa = numeroDaCamisa;
    this.posicao = posicao;
    this.dataDeNascimento = dataDeNascimento;
  }
  
  imprimirDadosDoJogador() {
    console.log(`Jogador
    
Nome: ${this.nome} 
Numero da Camisa: ${this.numeroDaCamisa}
Posição: ${this.posicao}
Data de Nascimento: ${this.dataDeNascimento} 
`);
  }
}

let jogador = new Jogador("Cássio", 12, "Goleiro", "6 de junho de 1987");
jogador.imprimirDadosDoJogador();


class Time {
  constructor(nome, localizacao, tecnico, titulos) {
    this.nome = nome;
    this.localizacao = localizacao;
    this.tecnico = tecnico;
    this.titulos = titulos;
  }
  
  imprimirDadosDoTime() {
    console.log(`Time

Nome: ${this.nome} 
Localização: ${this.localizacao}
Tecnico: ${this.tecnico}
Títulos(total): ${this.titulos}
`);
  }
}

let time = new Time("Sport Club Corinthians Paulista", "São Paulo/SP", "Fernando Lázaro", 55);
time.imprimirDadosDoTime();

class Arbitro {
  constructor(nivelDeExperiencia, dataDeNascimento, nome) {
    this.nivelDeExperiencia = nivelDeExperiencia;
    this.dataDeNascimento = dataDeNascimento;
    this.nome = nome;
  }
  
  imprimirDadosDoArbitro() {
    console.log(`Árbitro

Nivel de Experiencia: ${this.nivelDeExperiencia} 
Data de Nascimento: ${this.dataDeNascimento}
Nome: ${this.nome}
`);
  }
}

let arbitro = new Arbitro("desde 2013", "28 de dezembro de 1981", "Wilton Pereira Sampaio");
arbitro.imprimirDadosDoArbitro();


class Jogo {
  constructor(times, data, horario, placar, tempoDeJogo) {
    this.times = times
    this.data = data;
    this.horario = horario;
    this.placar = placar;
    this.tempoDeJogo = tempoDeJogo;
  ;
  }
  
  imprimirDadosDoJogo() {
    console.log(`Jogo
    
Times: ${this.times}    
Data de Realização: ${this.data} 
Horario de Realização: ${this.horario}
Placar: ${this.placar}
Tempo de Jogo: ${this.tempoDeJogo}`);
  }
}

let jogo = new Jogo("Campinense X Grêmio", "01/03/2023", "20h", "0 X 2", "96min");
jogo.imprimirDadosDoJogo();




